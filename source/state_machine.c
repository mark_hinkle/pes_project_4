#include "state_machine.h"
#include "i2c.h"

/* Global Vars */
static uint32_t raw_temp;
static float 	cur_temp;
static float 	avg_temp;
static float 	temp_totals;
static uint16_t num_readings;
static uint8_t  num_timeouts;

bool alerts_en;

SM_t stateMachineA(state_t * state)
{
	SM_t next_SM = SM_A;
	state_t next_state;
	I2CStatus_t ret;

	switch(*state)
	{
		case ST_TempReading:
			/* Read Sensor */
			ret = I2CReadTemperature(&raw_temp);
			if(ret == error)
			{
				next_state = ST_Disconnected;
				break;
			}

			/* Convert Temperature */
			cur_temp = convertTemp(raw_temp);
			if(cur_temp >= 0)
				next_state = ST_AvgWait;
			else
			{
				next_state = ST_Alert;
			}
			break;

		case ST_AvgWait:
			/* Calculate new avg value */
			calculateAverage(cur_temp);

			/* Wait 15 Seconds */
			//timeout();
			num_timeouts++;
			if(num_timeouts >= 4)
			{
				next_SM = SM_B;
			}
			next_state = ST_TempReading;

			break;

		case ST_Alert:
			alerts_en = false;
			break;

		case ST_Disconnected:
			next_SM = END;
			break;

		default:
			break;
	}

	*state = next_state;
	return next_SM;
}

SM_t stateMachineB(state_table_t * state)
{
	SM_t next_SM = SM_B;

	/* In Progress */

	return next_SM;
}

float convertTemp(uint32_t raw_data)
{
	/* Needs Work */
	float temp = -1.5 * raw_data;
	return temp;
}

void calculateAverage(float temp)
{
	num_readings++;
	temp_totals += temp;
	avg_temp = (temp_totals / num_readings);
}

void initSMParameters(state_table_t * state_b)
{
	raw_temp        = 0;
	cur_temp        = 0;
	avg_temp 		= 0;
	temp_totals 	= 0;
	num_readings 	= 0;
	num_timeouts 	= 0;
	alerts_en 		= true;

	state_b->cur_st 		= ST_TempReading;
	state_b->complete_st 	= ST_AvgWait;
	state_b->alert_st       = ST_Alert;
	state_b->disconnect_st  = ST_Disconnected;
	state_b->timeout_st     = ST_TempReading;
}
