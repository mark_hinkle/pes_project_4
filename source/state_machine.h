#ifndef _STATE_MACHINE_H
#define _STATE_MACHINE_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

typedef enum
{
	SM_A,
	SM_B,
	END
} SM_t;

typedef enum
{
	ST_TempReading,
	ST_AvgWait,
	ST_Alert,
	ST_Disconnected
} state_t;

typedef struct
{
	state_t cur_st;
	state_t complete_st;
	state_t alert_st;
	state_t disconnect_st;
	state_t timeout_st;
} state_table_t;


SM_t stateMachineA(state_t * state);
SM_t stateMachineB(state_table_t * state);
float convertTemp(uint32_t raw_data);
void calculateAverage(float temp);
void initSMParameters(state_table_t * state_b);

#endif /* _STATE_MACHINE_H */
